<?php
    echo 'Jawaban soal Praktik nomor 1 --->';
    echo "</br>";

    $result_1 = [];
    for ($i=0; $i <= 15 ; $i++) { 
        if ($i%2 != 0) {
            array_push($result_1, $i);
        }
    }

    echo(implode(",",$result_1));
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 2.i --->';
    echo "</br>";
    $date = "2020-11-23";
    echo date('m-y-d',strtotime($date));
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 2.ii --->';
    echo "</br>";
    $d = strtotime("+6 months",strtotime($date));
    echo date("m-y-d",$d);
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 2.iii --->';
    echo "</br>";  
    $bday = "1995-06-03";
    echo date_diff(date_create($date), date_create($bday))->y.' Tahun';
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 3.i --->';
    echo "</br>";  
    $array = [20,10,100,30,15,5];
    sort($array);
    print_r($array);
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 3.ii --->';
    echo "</br>";  
    array_push($array, 120);
    print_r($array);
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 3.iii --->';
    echo "</br>";  
    unset($array[1]);
    print_r($array);
    echo "</br></br>";

    echo 'Jawaban soal Praktik nomor 4 --->';
    echo "</br>";  
    $arrText = ['Aku', 'Suka', 'Makan', 'Nasi', 'Padang'];
    
    function balikinArray($arrSrc){
        $arrTemp = [];
        $arrDest = [];
        
        for ($i=0; $i <= count($arrSrc) ; $i++) { 
            for ($j=0; $j < $i ; $j++) { 
                array_push($arrTemp, $arrSrc[$j]);
            }
            array_push($arrDest, implode(' ',$arrTemp));
            $arrTemp = [];
        }
        echo '<pre>';
        var_dump($arrDest);
        echo '</pre>';
    }

    balikinArray($arrText);
    balikinArray(array_reverse($arrText));
    
?>